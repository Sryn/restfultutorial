# RESTfulTutorial README #

This is following an article titled 'How to create a RESTful web service using WCF' [here](http://www.developerhandbook.com/wcf/restful-web-service-using-wcf-1-of-3/).

Project Url set at [http://localhost:64307/](http://localhost:64307/)

Have to install EntityFramework v6.1.3 via Nuget for Data part to have using reference to System.Data.Entity and to compile.

And DropCreateDatabaseAlways needed a &lt;TContext&gt; of &lt;BlogContext&gt;.

Part 2:

For IBlogService, add reference to System.ServiceModel.Web into References 'folder' and then as using reference for WebGet.

Sryn
201702161430