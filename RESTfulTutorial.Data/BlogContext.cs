﻿using System.Data.Entity;

namespace RESTfulTutorial.Data
{
    public class BlogContext : DbContext
    {
        public BlogContext() : base("BlogContext")
        {

        }

        public DbSet<BlogPost> BlogPosts { get; set; }
    }
}
